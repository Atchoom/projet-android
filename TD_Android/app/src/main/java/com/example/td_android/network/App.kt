package com.example.td_android.network

import android.app.Application
import com.example.td_android.network.Api

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        Api.INSTANCE = Api(this)
    }
}