@file:Suppress("NAME_SHADOWING")

package com.example.td_android

import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.Toast

class AuthentificationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        handleIntent(intent)
        setContentView(R.layout.activity_authentification)
    }

    private fun handleIntent(intent: Intent){
        val action: String? = intent.action
        val data: Uri? = intent.data
        val authorisation = PreferenceManager.getDefaultSharedPreferences(this).getString(SHARED_PREF_TOKEN_KEY, "") != ""
        if(action == Intent.ACTION_VIEW){
            when (data?.lastPathSegment) {
                    "profile" -> {
                        if(authorisation) {
                            val intent = Intent(this, UserInfoActivity::class.java)
                            startActivity(intent)
                        }  else {
                            Toast.makeText(this,"You have to be authentified first",
                                3*Toast.LENGTH_SHORT).show()
                        }
                    }
                    "sign" -> {
                        //Implicit deep Link
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            intent.removeFlags(FLAG_ACTIVITY_NEW_TASK)
                        }
                    }
                    else -> {
                        if(authorisation) {
                            val intent = Intent(this, MainActivity::class.java)
                            startActivity(intent)
                        }  else {
                            Toast.makeText(this,"You have to be authentified first",
                                3*Toast.LENGTH_SHORT).show()
                        }
                    }
                }
        }
    }
}



