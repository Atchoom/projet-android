package com.example.td_android

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_task.view.*

class TaskViewHolder(itemView: View, val taskModel: TaskViewModel) : RecyclerView.ViewHolder(itemView) {
    fun bind(task: Task) {
        itemView.task_title.text = task.title
        itemView.task_description.text = task.description
        itemView.task_id.text = task.id

        itemView.button_delete.setOnClickListener { taskModel.deleteTask(task) }
        itemView.button_edit.setOnClickListener { edit(task) }
    }
    private fun edit(task: Task){
        val intent = Intent(itemView.context,TaskActivity::class.java)
        intent.putExtra("id",task.id)
        intent.putExtra("title",task.title)
        intent.putExtra("description",task.description)
        itemView.context.startActivity(intent)
    }
}