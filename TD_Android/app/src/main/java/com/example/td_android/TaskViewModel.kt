package com.example.td_android

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.td_android.network.TaskRepository
import kotlinx.coroutines.launch

class TaskViewModel: ViewModel() {
    private val repository = TaskRepository()
    private val tasks = mutableListOf<Task>()


    val taskAdapter = TaskAdapter(tasks,this)

    fun deleteTask (task : Task){
        viewModelScope.launch {
            if(repository.deleteTask(task)){
                tasks.remove(task)
                taskAdapter.notifyDataSetChanged()
            }
        }
    }

    fun loadTasks() {
        viewModelScope.launch{
            val repotasks = repository.loadTasks()
            if(repotasks != null){
                tasks.clear()
                tasks.addAll(repotasks)
                taskAdapter.notifyDataSetChanged()
            }
        }
    }

    fun createTask(task : Task)
    {
        viewModelScope.launch{
            if(repository.createTask(task)){
                tasks.add(task)
                taskAdapter.notifyDataSetChanged()
            }
        }
    }

    fun editTask(task : Task){
        viewModelScope.launch{
            if(repository.editTask(task)){
                var upTask= tasks.find{it.id == task.id}
                if (upTask != null) {
                    upTask.title = task.title
                }
                if (upTask != null) {
                    upTask.description = task.description
                }
                taskAdapter.notifyDataSetChanged()

            }
        }
    }
}