package com.example.td_android.network

import android.content.Context
import android.preference.PreferenceManager
import com.example.td_android.SHARED_PREF_TOKEN_KEY
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class Api(private val context: Context) {
    companion object {
        private const val BASE_URL = "https://android-tasks-api.herokuapp.com/api/"
        lateinit var INSTANCE: Api
    }

    private val moshi = Moshi.Builder().build()

    private val okHttpClient by lazy {
        OkHttpClient.Builder()
            .addInterceptor { chain ->
                val newRequest = chain.request().newBuilder()
                    .addHeader("Authorization", "Bearer "+getToken())
                    .build()
                chain.proceed(newRequest)
            }
            .build()
    }

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()

    val userService: UserService by lazy { retrofit.create(UserService::class.java) }
    val taskService: TaskService by lazy { retrofit.create(TaskService::class.java) }

    private fun getToken() : String{
        val token = PreferenceManager.getDefaultSharedPreferences(context).getString(SHARED_PREF_TOKEN_KEY, "") ?: ""
        return token//"eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxNTYsImV4cCI6MTYwOTY3NTU4MH0.HC34ZEHzA8zWQ-rgvLSg3NyrKuDVAVT37bh1QqAH5mc"
        //token
    }
}
/*
import android.content.Context
import android.preference.PreferenceManager
import com.example.td_android.SHARED_PREF_TOKEN_KEY
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class Api(private val context: Context) {
    companion object {
        private const val BASE_URL = "https://android-tasks-api.herokuapp.com/api/"
        //private const val TOKEN =
        //    "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjozNCwiZXhwIjoxNjA3NTI0ODY0fQ.ioQl0e-8vOuYfiETlHIA2OT8Oqkhe2BwXA86lnVQzG8"
        lateinit var INSTANCE: Api
    }
    private val moshi = Moshi.Builder().build()

    private val okHttpClient by lazy {
        OkHttpClient.Builder()
            .addInterceptor {chain ->
                val newRequest = chain.request().newBuilder()
                    .addHeader("Authorization", "Bearer ${getToken()}")
                    .build()
                chain.proceed(newRequest)
            }
            .build()
    }

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()

    fun getToken() : String {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(SHARED_PREF_TOKEN_KEY, "") ?: ""
    }


    val userService: UserService by lazy { retrofit.create(UserService::class.java)}
    val taskService: TaskService by lazy { retrofit.create(TaskService::class.java)}

}*/