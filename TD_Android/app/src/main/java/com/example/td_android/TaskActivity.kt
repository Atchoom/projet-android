package com.example.td_android

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_task.*

class TaskActivity : AppCompatActivity() {

    private val taskViewModel by lazy {
        ViewModelProviders.of(this).get(TaskViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)
        text_title_task.setText(intent.getStringExtra("title") ?: "")
        description_text_task.setText(intent.getStringExtra("description") ?: "")
        val id = intent.getStringExtra("id")
        if (id == null) {
            button_ok.setOnClickListener { createTask() }
        } else {
            button_ok.setOnClickListener { editTask(id) }
        }
        button_cancel.setOnClickListener { goBack() }
    }

    private fun createTask() {
        var title = text_title_task.text.toString()
        var description = description_text_task.text.toString()
        if (title != "" && description != "") {
            var id = "" + title.hashCode() + description.hashCode()
            val newTask = Task(id, title, description)
            taskViewModel.createTask(newTask)
        }

        goBack()
    }

    private fun editTask(id: String) {
        var title = text_title_task.text.toString()
        var description = description_text_task.text.toString()
        if (title != "" && description != "") {
            val newTask = Task(id, title, description)
            taskViewModel.editTask(newTask)
        }
        goBack()
    }

    private fun goBack() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}
