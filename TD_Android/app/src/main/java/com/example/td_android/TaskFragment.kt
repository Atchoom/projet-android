package com.example.td_android

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.Settings.Global.putString
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.task_fragment.view.*


class TaskFragment : Fragment(){
    //private val tasksRepository = TaskRepository()
    //private val tasks = mutableListOf<Task>()
    //private val taskAdapter = TaskAdapter(tasks)

    private val taskViewModel by lazy {
        ViewModelProviders.of(this).get(TaskViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =  inflater.inflate(R.layout.task_fragment,container)
        view.task_view.adapter = taskViewModel.taskAdapter
        view.task_view.layoutManager = LinearLayoutManager(context)
        view.button_create.setOnClickListener { createTask() }
        view.button_logout.setOnClickListener { logOut() }
        return view
    }

    override fun onResume() {
        super.onResume()
        taskViewModel.loadTasks()
    }

    private fun createTask(){
        val intent = Intent(this.context,TaskActivity::class.java)
        this.context?.startActivity(intent)
    }

    private fun logOut(){
        PreferenceManager.getDefaultSharedPreferences(this.context).edit{
            putString(SHARED_PREF_TOKEN_KEY,"")
        }
        val intent = Intent(this.context,AuthentificationActivity::class.java)
        this.context?.startActivity(intent)
    }




}