package com.example.td_android

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.td_android.network.Api
import com.example.td_android.network.UserInfo
import com.example.td_android.network.UserService
import kotlinx.android.synthetic.main.header_fragment.*
import kotlinx.android.synthetic.main.header_fragment.view.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class HeaderFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.header_fragment,container)
            view.image_view.setOnClickListener{goToUserInfoActivity()}
        return view
    }

    private val coroutineScope = MainScope()

    override fun onResume() {
        coroutineScope.launch {
            val info =  Api.INSTANCE.userService.getInfo()
            Glide.with(this@HeaderFragment).load(info.body()?.avatar).apply(RequestOptions.circleCropTransform()).into(image_view)
            user_text.text =info.body()?.firstName + info.body()?.lastName
        }
        super.onResume()
    }

    private fun goToUserInfoActivity(){
        val userActivity = Intent(activity,UserInfoActivity::class.java)
        startActivity(userActivity)
    }
}