package com.example.td_android

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_authentification.*
import kotlinx.android.synthetic.main.fragment_authentification.view.*

class AuthentificationFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_authentification,null)
        view.signUp_button.setOnClickListener{goToSignUpFragment()}
        view.logIn_button.setOnClickListener {goToLoginFragment()}
        return view
    }

    private fun goToLoginFragment(){
        findNavController().navigate(R.id.action_authentificationFragment_to_loginFragment2)
    }
    private fun goToSignUpFragment(){
        findNavController().navigate(R.id.action_authentificationFragment_to_signupFragment2)
    }
}
