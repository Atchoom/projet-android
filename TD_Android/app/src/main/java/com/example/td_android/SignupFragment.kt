package com.example.td_android

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.td_android.network.Api
import kotlinx.android.synthetic.main.fragment_authentification.view.*
import kotlinx.android.synthetic.main.fragment_signup.*
import kotlinx.android.synthetic.main.fragment_signup.view.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class SignupFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_signup, null)
        view.signUp.setOnClickListener { onSignUpButtonClick() }
        view.signUp_backButton.setOnClickListener { goBack() }
        return view
    }

    private val coroutineScope = MainScope()

    private fun goToAuthentification() {
        val intent = Intent(context, AuthentificationActivity::class.java)
        startActivity(intent)
    }

    private fun onSignUpButtonClick() {
        val firstname = signUpNameText.text
        val lastname = signUpLastNameText.text
        val email = signUpEmail.text
        val password = signUpPasswordText.text
        val passwordConfirm = signUpPasswordConfirmText.text

        if (firstname.isBlank() || lastname.isBlank() || email.isBlank() || password.isBlank() || passwordConfirm.isBlank()) {
            Toast.makeText(context, "Champs vides", Toast.LENGTH_LONG).show()
            return
        }
        if (password.toString() != passwordConfirm.toString()) {
            Toast.makeText(context, "Password not confirmed", Toast.LENGTH_LONG).show()
            return
        }

        val signUpForm = SignUpForm(
            firstname.toString(),
            lastname.toString(),
            email.toString(),
            password.toString(),
            passwordConfirm.toString()
        )
        coroutineScope.launch {
            val response = Api.INSTANCE.userService.signup(signUpForm)
            if (response.isSuccessful) {
                goToAuthentification()
            } else {
                Toast.makeText(context, "Error: " + response.errorBody(),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun goBack(){
        findNavController().navigate(R.id.action_signupFragment_to_authentificationFragment)
    }
}
