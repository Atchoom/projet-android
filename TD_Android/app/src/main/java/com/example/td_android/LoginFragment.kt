package com.example.td_android

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.Settings.Global.putString
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.edit
import androidx.navigation.fragment.findNavController
import com.example.td_android.network.Api
import com.example.td_android.network.UserService
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.view.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class LoginFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_login, null)
        view.logInButton.setOnClickListener{onLogInButtonClick()}
        view.logIn_backButton.setOnClickListener { goBack() }
        return view
    }

    private val coroutineScope = MainScope()

    private fun onLogInButtonClick(){

        if(logInEmailText.text.isBlank() || logInPasswordText.text.isBlank()){
            Toast.makeText(context, "Champs vides", Toast.LENGTH_LONG).show()
            return
        }
        val lgForm = LoginForm(logInEmailText.text.toString(),logInPasswordText.text.toString())
        coroutineScope.launch{
            val response = Api.INSTANCE.userService.login(lgForm)
            if( response.isSuccessful){
                PreferenceManager.getDefaultSharedPreferences(this@LoginFragment.context).edit{
                    putString(SHARED_PREF_TOKEN_KEY, response.body()?.token)
                }
                val intent = Intent(activity?.applicationContext, MainActivity::class.java)
                startActivity(intent)
            }
            else
            {
                Toast.makeText(this@LoginFragment.context,"Error: " + response.errorBody(),Toast.LENGTH_SHORT).show()

            }
        }
    }

    private fun goBack(){
        findNavController().navigate(R.id.action_loginFragment_to_authentificationFragment)
    }
}
