package com.example.td_android.network

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.td_android.Task
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class TaskRepository {
    private val tasksService = Api.INSTANCE.taskService
    private val coroutineScope = MainScope()

    suspend fun loadTasks(): List<Task>? {
        val tasksResponse = tasksService.getTasks()
        Log.e("loadTasks", tasksResponse.toString())
        return if (tasksResponse.isSuccessful) tasksResponse.body() else null
    }

    suspend fun deleteTask(task : Task) :  Boolean {
        val tasksResponse = tasksService.deleteTask(task.id)
        return tasksResponse.isSuccessful
    }

    suspend fun createTask(task : Task) : Boolean {
        val tasksResponse = tasksService.createTask(task)
        return tasksResponse.isSuccessful
    }

    suspend fun editTask(task : Task) : Boolean {
        val tasksResponse = tasksService.updateTask(task.id,task)
        return tasksResponse.isSuccessful
    }
}